import config
import importlib
import asyncio

from munch import Munch

from notifier.xsoverlay_api import send_message


def load_modules(module_list: dict):
    modules = Munch()
    for module_category in module_list:
        module_category_items = module_list[module_category]

        for module in module_category_items:
            modules[module_category] = {}
            modules[module_category][module] = importlib.import_module(
                f".{module_category}.{module}", package="notifier.modules"
            )

    return modules


def run_watchdogs(loop, modules):
    for module_category in modules:
        module_category_items = modules[module_category]

        for module_name in module_category_items:
            module = modules.get(module_category).get(module_name)
            loop.create_task(
                module.run_watchdog(
                    config.MODULES_CONFIG[module_category][module_name]["watchfile"]
                )
            )


def main():
    modules = load_modules(config.ENABLED_MODULES)
    loop = asyncio.get_event_loop()

    run_watchdogs(loop, modules)

    loop.run_forever()


if __name__ == "__main__":
    main()
