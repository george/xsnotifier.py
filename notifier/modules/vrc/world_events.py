import config
import asyncio

from watchgod import awatch
from ...xsoverlay_api import send_message


async def scan_line(line):
    splat = line.split(" ")
    if "OnPlayerJoined" in splat:
        username = splat[-1].replace("\n", "")
        if username != config.MODULES_CONFIG["vrc"]["world_events"]["my_username"]:
            send_message("Played joined.", f"{username} has joined the room!")

    elif "OnPlayerLeft" in splat:
        username = splat[-1]
        if username != config.MODULES_CONFIG["vrc"]["world_events"]["my_username"]:
            send_message("Played left.", f"{username} has left the room!")


async def log_changed(file):
    loop = asyncio.get_event_loop()
    with open(file) as file:
        for line in file.readlines()[-8:]:
            loop.create_task(scan_line(line))


async def run_watchdog(watchfile: str):
    print(watchfile)
    async for changes in awatch(watchfile):
        await log_changed(watchfile)
