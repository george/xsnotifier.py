import os
import glob
from pathlib import Path


def get_latest_logfile():
    WINDOWS_VRCHAT_LOG_PATH = f"{os.getenv('APPDATA')}\\..\\LocalLow\\VRChat\\VRChat"
    files_listing = glob.glob(os.path.expanduser(f"{WINDOWS_VRCHAT_LOG_PATH}\\*"))
    files = sorted(files_listing, key=lambda t: -os.stat(t).st_mtime)

    log_file = ""
    for file in files:
        if "output_" in file:
            log_file = file
            break

    return log_file
