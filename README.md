### XSNotifier[.py]

This is a Python implementation of the C# based [XSNotifications](https://github.com/nnaaa-vr/XSNotifications) library, as well as the [VRChat Log parser for it](https://github.com/nnaaa-vr/XSOverlay-VRChat-Parser).

Currently implemented are join and leave notifications for VRChat, as well as a base `send_message` function, which can be used by any other module.

There's also a module autoloader implemented, just drop in your module code, add it to the config, and it should be loaded.
