import json
import config
import socket


def send_message(
    title: str,
    content: str,
    audio: str = "default",
    icon: str = "default",
    icon_is_b64: bool = False,
    timeout: float = 0.5,
    volume: float = 1,
):
    message = {
        "messageType": 1,
        "index": 0,
        "timeout": timeout,
        "height": 175,
        "opacity": 1,
        "volume": volume,
        "audioPath": audio,
        "title": title,
        "content": content,
        "useBase64Icon": icon_is_b64,
        "icon": icon,
        "sourceApp": "XSNotifier.py",
    }
    the_cum_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    message = bytes(json.dumps(message), "utf-8")
    the_cum_sock.sendto(message, ("127.0.0.1", config.XS_PORT))
