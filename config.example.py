# this is a hardcoded module import, as I was lazy.
from notifier.modules.vrc.utils import log_utils

XS_PORT = 42069
ENABLED_MODULES = {"vrc": ["world_events"]}
MODULES_CONFIG = {
    "vrc": {
        "world_events": {
            "watchfile": log_utils.get_latest_logfile(),
            "my_username": "",  # Your VRChat username.
        }
    }
}

